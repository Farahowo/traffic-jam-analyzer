<?php
  require_once("../includes/head.php");

  $success = false; //sign up success flag
  $mismatch = false; //username password mismatch flag

  if (isset($_POST['signup'])) {
    $data = array();

    $data[0] = escape($_POST['fname']);
    $data[1] = escape($_POST['lname']);
    $data[2] = escape($_POST['username']);
    $data[3] = escape($_POST['email']);
    $data[4] = escape($_POST['password']);
    $hashed_password = crypt($data[4],'st');
    $data[5] = escape($_POST['security_q']);
    $data[6] = escape($_POST['security_a']);

    $query= "INSERT INTO `USER` VALUES (NULL,'$data[0]','$data[1]','$data[3]','$data[2]','$data[5]','$data[6]','$hashed_password',0,NULL,0);";
    $res= query($query);

    if ($res) {
      $success = true;
    }
  } elseif (isset($_POST['signin'])) {
      $username = $_POST['username'];
      $password = $_POST['password'];
      $user_input = crypt($password,'st');

      $query = "SELECT password FROM `USER` WHERE username = '$username'";
      $res = query($query);
      $out = mysqli_fetch_array($res);

      if ($user_input == $out[0]) {
        $_SESSION['username'] = $username;
        jump("/");
      } else {
        $mismatch = true;
      }
  } elseif (isset($_GET['mode'])) { //LOGOUT
    if($_GET['mode'] == "logout") {
      unset($_SESSION['username']);
      $USERNAME = NULL;
    }
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Login / Sign Up</title>

    <!-- Bootstrap -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>
  </head>
  
  <body>
<?php require_once("../includes/header.php"); ?> 
      <div class="container center-div" style="margin-top: 150px;">
        <div class="col-lg-6 col-lg-offset-4 col-md-5 col-md-offset-4 col-sm-8 col-sm-offset-2">
<?php
  if ($success) {
?>
  <div class="alert alert-success fade in">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    Account created successfully!
  </div>
<?php
  }          
?>        
        <ul class="nav nav-tabs col-lg-7">
          <li class="active"><a data-toggle="tab" href="#signin">Sign In</a></li>
          <li><a data-toggle="tab" href="#signup">Sign Up</a></li>
        </ul>

        <div class="tab-content" style="margin-top: 30px;">
        
          <div id="signin" class="tab-pane fade in active">

             <!-- Sign In --> 

              <form action="/sign-in-up/index.php" method="post" class="form-horizontal col-lg-6 col-md-5 col-sm-8" style="margin: 30px">
<?php
  if ($mismatch) {
?>
  <div class="alert alert-danger fade in">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    Incorrect username or password.
  </div>

<?php }
  if (isset($_GET['error'])) {
    if ($_GET['error'] == "notuser") {
?>
  <div class="alert alert-info fade in">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    You must log in first.
  </div>
<?php
    }
  }

?>             
                <div class="form-group">
                  <label for="username">Username:</label>
                  
                    <input type="text" name="username" class="form-control" id="username" placeholder="Enter username">
                  
                </div>

                <div class="form-group">
                  <label for="pwd">Password:</label>
                  
                    <input type="password" name="password" class="form-control" id="pwd" placeholder="Enter password">
                  
                </div>

                <div class="form-group">
                  
                    <div class="checkbox">
                      <label><input type="checkbox"> Remember me</label>
                    </div>
                  
                </div>

                <div class="form-group">
                    <input type="submit" name="signin" class="btn btn-success btn-lg btn-block" value="LOGIN">
                </div>

                <div class="form-group">
                  
                    <a href="#">Did You Forget Your Username?</a>
                
                </div>

                <div class="form-group">
                  
                    <a href="/traffic">Traffic police</a>
                
                </div>
              </form>
          </div>

          <div id="signup" class="tab-pane fade">

             <!-- Sign Up Form -->
            
            <form action="/sign-in-up/index.php" method="post" class="form-horizontal col-lg-6 col-md-5 col-sm-8" style="margin: 30px" >        

                <div class="form-group">
                  <label for="fname">First Name:</label>
                  
                    <input type="text" name="fname" class="form-control" id="fname" placeholder="Enter first name">
                  
                </div>

                <div class="form-group">
                  <label for="lname">Last Name:</label>
                  
                    <input type="text" name="lname" class="form-control" id="lname" placeholder="Enter last name">
                  
                </div>

                <div class="form-group">
                  <label for="username">Username:</label>
                  
                    <input type="text" name="username" onkeyup="checkUsername(this.value)" class="form-control" id="username" placeholder="Enter username">
                    <p id="unavailable" style="color: red;"></p>
                
                </div>

                <div class="form-group">
                  <label for="email">Email:</label>
                  
                    <input type="email" name="email" class="form-control" id="email" placeholder="Enter email">
                  
                </div>

                <div class="form-group">
                  <label for="pwd">Password:</label>
                
                    <input type="password" name="password" id="password" class="form-control" id="pwd" placeholder="Enter password">
                
                </div>

                <div class="form-group">
                  <label for="confpwd">Confirm Password:</label>
                
                    <input type="password" class="form-control" id="confpwd" onblur="checkPass()" placeholder="Confirm password">
                    <p id="pass_mismatch" style="color: red;"></p>
                </div>

                <div class="form-group">
                  <label for="ques">Security Question:</label>
                  
                    <input type="text" name="security_q" class="form-control" id="ques" placeholder="E.g. Your mother's maiden name?">
                  
                </div>

                <div class="form-group">
                  <label for="ans">Answer:</label>
                  
                    <input type="text" name="security_a" class="form-control" id="ans" placeholder="Answer to the security question">
                  
                </div>

                <div class="form-group">
                    <input type="submit" name="signup" class="btn btn-success btn-lg btn-block" value="SIGNUP">
                </div>

              </form>

          </div>
  
        </div>

        </div>
  
      </div>	
    
    <script type="text/javascript">
      // Javascript to enable link to tab
      var url = document.location.toString();
      if (url.match('#')) {
          $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
      } 

      // Change hash for page-reload
      $('.nav-tabs a').on('shown.bs.tab', function (e) {
          window.location.hash = e.target.hash;
      })
    </script>
    <script type="text/javascript">
      function checkPass() {
        var pass = document.getElementById('password').value;
        var confPass = document.getElementById('confpwd').value;

        if (pass != confPass) {
          document.getElementById('pass_mismatch').innerHTML = "Password doesn't match";
        } else {
          document.getElementById('pass_mismatch').innerHTML = "";
        }
      }

      function checkUsername(username) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange= function() {
          if (this.readyState == 4 && this.status == 200) {
            var xmlDoc = this.responseXML;
            var result = xmlDoc.getElementsByTagName("username");

            var output = result[0].firstChild.nodeValue;
            if (output == "unavailable") {
              document.getElementById('unavailable').innerHTML= "<span class=\"glyphicon glyphicon-remove\"></span> Username not available.";
            } else {
              document.getElementById('unavailable').innerHTML= "";
            }
          }
        }
        xhttp.open("GET", "/sign-in-up/ajax.php?username="+username, true);
        xhttp.send();

      }
    </script>
  </body>

</html>