<?php require_once("../includes/head.php"); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Live Search Assignment</title>

    <!-- Bootstrap -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <style type="text/css">
    	body {
    		background-image: url('traffic.jpg');
		    background-repeat: no-repeat;
		    background-attachment: fixed;
		    background-size: cover;
    	}
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>

    <script type="text/javascript">
      function search(keyword) {
        if (keyword.length == 0) {
          document.getElementById("output").innerHTML = "";
          return;
        }
        
        var xhttp = new XMLHttpRequest();

        xhttp.onreadystatechange= function() {
          if (this.readyState == 4 && this.status == 200) {
            var xmlDoc = this.responseXML;
            var results = xmlDoc.getElementsByTagName("content");

            document.getElementById("output").innerHTML = "";
            var div = document.createElement('div');
            div.className = "list-group-item";
            document.getElementById("output").appendChild(div);

            if (results.length == 0) {
              div.innerHTML = "<strong>No results found</strong>";
            }

            for (var i = 0; i < results.length; i++) {
              var a = document.createElement('a');
              var data = results[i].firstChild.nodeValue;
              
              a.appendChild(document.createTextNode(data));
              a.href = "#";
              a.className = "list-group-item list-group-item-action";
              
              div.appendChild(a);
            }

          }
        };

        xhttp.open("GET", "/res/response.php?key="+keyword, true);
        xhttp.send();
        //document.getElementById("results").innerHTML = keyword;
      }
    </script>
  </head>
<body>
<?php require_once("../includes/header.php") ?>
                
  <form style="margin-top: 200px" class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1">
    
    <!--<input type="text" class="form-control">
    <div>
      <ul class="list-group">
        <li class="list-group-item">
          <a href="#" class= "list-group-item-action">jhjhjh</a>
        </li>
        <li class="list-group-item">
          <a href="#" class= "list-group-item-action">jhjhjh</a>
        </li>
        <li class="list-group-item">
          <a href="#" class= "list-group-item-action">jhjhjh</a>
        </li>
      </ul>
    </div>-->

    <input type="text" onkeyup="search(this.value)" class="form-control" placeholder="Search anything...">

    <div id="output">
      
    </div>

  </form>
 
	                       
</body>
</html>