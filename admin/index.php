<?php
	require_once("../includes/head.php");

	$success = true;

	if (isset($_POST['submit'])) {
		$id = $_POST['id'];
		$password = $_POST['password'];

		$query = "SELECT password FROM `ADMIN` WHERE username = '$id'";
		$res = query($query);
		$out = mysqli_fetch_array($res);

		if ($password == $out[0]) {
			$_SESSION['admin_id'] = $id;
			jump("/admin/panel");
		} else {
			$success = false;
		}
	}

  if (isset($_GET['mode'])) {
    if ($_GET['mode'] == "logout") {
      $_SESSION['admin_id'] = NULL;
      header("Location: /admin");
    }
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Admin Login</title>

    <!-- Bootstrap -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>
  </head>
  
  <body>
    <div class="container center-div" style="margin-top: 150px;">
      <div class="col-lg-6 col-lg-offset-4 col-md-5 col-md-offset-4 col-sm-8 col-sm-offset-2">
      <!-- Sign In --> 
<?php if (!$success) {
?>
  <div class="alert alert-danger fade in">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    Incorrect username or password.
  </div>
<?php }
?>           
        <form action="/admin/index.php" method="post" class="form-horizontal col-lg-6 col-md-5 col-sm-8" style="margin: 30px">
          <h2 align="center">Admin login</h2><hr>
          <div class="form-group">
            <label for="ID">ID:</label>
            
              <input type="text" name="id" class="form-control" id="ID" placeholder="Enter ID">
          </div>

          <div class="form-group">
            <label for="pwd">Password:</label>
            
              <input type="password" name="password" class="form-control" id="pwd" placeholder="Enter password">
          </div>

          

          <div class="form-group">
            <input type="submit" name="submit" value="LOGIN" class="btn btn-success btn-lg btn-block">
          </div>

        </form>
      </div>

    </div>
  </body>

</html>