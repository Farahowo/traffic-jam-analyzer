<?php
	require_once("../../includes/head.php");

	if (isset($_SESSION['admin_id'])) {
		$ADMINID = $_SESSION['admin_id'];
	} else {
		jump("/admin");
	}

	if (isset($_POST['add_traffic'])) {
		$username = $_POST['username'];
		$password = $_POST['password'];
		$phone = $_POST['phone'];
		$privilege = $_POST['privilege'];

		$query = "INSERT INTO TRAFFIC_POLICE VALUES (NULL, '$username','$password',$privilege,'$phone')";
		query($query);
	} elseif (isset($_POST['add_city'])) {
		$city = $_POST['name'];

		$query = "INSERT INTO CITY VALUES (NULL, '$city')";
		query($query);
	} elseif (isset($_POST['add_area'])) {
		$area = $_POST['name'];
		$city_id = $_POST['city_id'];

		$query = "INSERT INTO AREA VALUES (NULL, '$area', $city_id)";
		query($query);
	} elseif (isset($_POST['add_place'])) {
    $place = $_POST['name'];
    $area_id = $_POST['area_id'];
    $lat_from = $_POST['lat_from'];
    $lat_to = $_POST['lat_to'];
    $long_from = $_POST['long_from'];
    $long_to = $_POST['long_to'];

    $query = "INSERT INTO PLACE VALUES (NULL,'$place',$area_id,$lat_from,$long_from,$lat_to,$long_to)";
    query($query);
  } elseif (isset($_POST['add_road'])) {
    $name = $_POST['name'];
    $from = $_POST['from_place'];
    $to = $_POST['to_place'];
    $distance = $_POST['distance'];

    $query = "INSERT INTO ROAD VALUES (NULL, '$name', $from, $to, $distance)";
    query($query);
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Admin Panel</title>

    <!-- Bootstrap -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>
  </head>

  <body data-spy="scroll" data-target=".navbar" data-offset="50">

    <nav class="navbar navbar-inverse navbar-fixed-top"> 
    <!-- If navbar-fixed-top is cut then the <h1> is shown. Else it cvers it. So <br> is used.--> 
    <!-- Navbar starts -->
      <div class="container-fluid">

        <div class="navbar-header">

          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>

          <a class="navbar-brand" href="#">TrafficNow</a> <!-- Give this site a name please -->
        </div>

        <div class="collapse navbar-collapse" id="myNavbar">
          <ul class="nav navbar-nav">
            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Traffic Police <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="#trafpol">Add Traffic Police Account</a></li>
                <li><a href="#trafpollist">List of Traffice Police Account</a></li>
              </ul>
            </li>
            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">City <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="#addcity">Add City</a></li>
              </ul>
            </li>
            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Area <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="#addarea">Add Area</a></li>
                <li><a href="#arealist">List of Area</a></li>
              </ul>
            </li>
            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Place <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="#addplace">Add Place</a></li>
                <li><a href="#placelist">List of Places</a></li>
              </ul>
            </li>
            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Road <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="#addroad">Add Road</a></li>
                <li><a href="#roadlist">List of Roads</a></li>
              </ul>
            </li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="/admin?mode=logout"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
          </ul>

        </div>

      </div>
    
    </nav>
    <!-- Navbar Ends -->

    <div class="container" style="margin-top: 70px">
      
      <div id = "trafpol" class="col-md-7 col-md-offset-2">
        <form action="/admin/panel/index.php" method="post">
          <div class="form-group">
            <h3>Add Traffic Police:</h3>
          </div>
          <div class="form-group">
            <label for="username">Username:</label>
            <input type="text" class="form-control" id="username" name="username">
          </div>
          <div class="form-group">
            <label for="password">Password:</label>
            <input type="password" class="form-control" id="password" name="password">
          </div>
          <div class="form-group">
            <label for="previlege">Privilege:</label>
            <input type="text" class="form-control" id="previlege" name="privilege">
          </div>
          <div class="form-group">
            <label for="phone">Phone:</label>
            <input type="text" class="form-control" id="phone" name="phone">
          </div>
          <div class="form-group">
            <input type="submit" name="add_traffic" class="btn btn-primary btn-lg form-control" value="SUBMIT">
          </div>
        </form>

        <div id="trafpollist">
          <h3>List of Traffic Police:</h3>
        </div>
        <ul class="list-group">
<?php
	$query = "SELECT * FROM TRAFFIC_POLICE";
	$res = query($query);
	while($out = mysqli_fetch_array($res)) {
?>
          <li class="list-group-item">
          	<?php echo $out['TP_ID'] .". ". $out['USERNAME'] ." : " . $out['PRIVILEGE']; ?>
          	<button type="button" class="btn btn-danger pull-right">Delete</button>
          </li>
<?php } ?>
        </ul>
      </div>

      <div id="addcity" class="col-md-7 col-md-offset-2">
        <form action="/admin/panel/index.php" method="post">
          <div class="form-group">
            <h3>Add City:</h3>
          </div>
          <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" class="form-control" name="name" id="name">
          </div>
          <div class="form-group">
            <input type="submit" name="add_city" class="btn btn-primary btn-lg form-control" value="SUBMIT">
          </div>
        </form>
      </div> 

      <div id="addarea" class="col-md-7 col-md-offset-2">
        <form action="/admin/panel/index.php" method="post">
          <div class="form-group">
            <h3>Add Area:</h3>
          </div>
          <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" class="form-control" name="name" id="name">
          </div>
          <div class="form-group">
            <label for="city">City:</label>
            <select class="selectpicker form-control" name="city_id">
<?php
	$query = "SELECT * FROM CITY";
	$res = query($query);
	while($out = mysqli_fetch_array($res)) {
?>
              <option value="<?php echo $out['CITY_ID'];?>"><?php echo $out['NAME'] ?></option>
<?php } ?>
            </select>
          </div>
          <div class="form-group">
            <input type="submit" name="add_area" class="btn btn-primary btn-lg form-control" value="SUBMIT">
          </div>
        </form>

        <div id="arealist">
          <h3>List of Areas:</h3>
        </div>
        <ul class="list-group">
<?php
	$query = "SELECT a.name, c.name FROM AREA a JOIN CITY c ON a.city_id = c.city_id";
	$res = query($query);
	while($out = mysqli_fetch_array($res)) {
?>        
          <li class="list-group-item">
          	<?php echo "<strong>$out[0]</strong>, $out[1]"; ?>
          	<button type="button" class="btn btn-danger pull-right">Delete</button>
          </li>
 <?php } ?>         
        </ul>
      </div>  

      <div id="addplace" class="col-md-7 col-md-offset-2">
        <form action="/admin/panel/index.php" method="post">
          <div class="form-group">
            <h3>Add Place:</h3>
          </div>

          <div id="map" style="width:100%;height:500px"></div> <!--Map goes here-->
          <button type="button" id="togglemarker" onclick="toggle()">Set location (To)</button> <!--Don't change the button ID-->

          <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" class="form-control" name="name" id="name">
          </div>
          <div class="form-group">
            <label for="area">Area:</label>
            <select class="selectpicker form-control" name="area_id">
<?php
	$query = "SELECT a.area_id, a.name, c.name FROM AREA a JOIN CITY c ON a.city_id = c.city_id";
	$res = query($query);
	while($out = mysqli_fetch_array($res)) {
?>
              <option value="<?php echo $out[0]; ?>"><?php echo "$out[1], $out[2]"; ?></option>
<?php } ?>
            </select>
          </div>
          <div class="form-group">
            <label for="lat_from">Latitude (from):</label>
            <input type="text" class="form-control" name="lat_from" id="lat_from">
          </div>
          <div class="form-group">
            <label for="lat_to">Latitude (to):</label>
            <input type="text" class="form-control" name="lat_to" id="lat_to">
          </div>
          <div class="form-group">
            <label for="long_from">Longitude (from):</label>
            <input type="text" class="form-control" name="long_from" id="long_from">
          </div>
          <div class="form-group">
            <label for="long_to">Longitude (to):</label>
            <input type="text" class="form-control" name="long_to" id="long_to">
          </div>
          <div class="form-group">
            <input type="submit" name="add_place" value="SUBMIT" class="btn btn-primary btn-lg form-control">
          </div>
        </form>

        <div id="placelist">
          <h3>List of Places:</h3>
        </div>
        <ul class="list-group">
<?php
  $query = "SELECT p.name, a.name FROM PLACE p JOIN AREA a ON p.area_id = a.area_id";
  $res = query($query);
  while($out = mysqli_fetch_array($res)) {
?>
          <li class="list-group-item"><?php echo $out[0].", ". $out[1]; ?>
            <button type="button" class="btn btn-danger pull-right">Delete</button>
          </li>
<?php } ?>
        </ul>
      </div>

      <div id="addroad" class="col-md-7 col-md-offset-2">
        <form action="/admin/panel/index.php" method="post">
          <div class="form-group">
            <h3>Add Road:</h3>
          </div>
          <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" class="form-control" name="name" id="name">
          </div>
          <div class="form-group">
            <label for="place_from">List of place (from):</label>
            <select class="selectpicker form-control" name="from_place">
<?php
  $query = "SELECT * FROM PLACE";
  $res = query($query);
  while($out = mysqli_fetch_array($res)) {
?>
              <option value="<?php echo $out['PLACE_ID'] ?>"><?php echo $out['NAME']; ?></option>
<?php } ?>
            </select>
          </div>
          <div class="form-group">
            <label for="place_to">List of place (to):</label>
            <select class="selectpicker form-control" name="to_place">
<?php
  $query = "SELECT * FROM PLACE";
  $res = query($query);
  while($out = mysqli_fetch_array($res)) {
?>
              <option value="<?php echo $out['PLACE_ID'] ?>"><?php echo $out['NAME']; ?></option>
<?php } ?>
            </select>
          </div>
          <div class="form-group">
            <label for="lat_from">Distance (in km):</label>
            <input type="text" class="form-control" name="distance" id="distance">
          </div>
          <div class="form-group">
            <input type="submit" name="add_road" value="SUBMIT" class="btn btn-primary btn-lg form-control">
          </div>
        </form>

        <div id="roadlist">
          <h3>List of Roads:</h3>
        </div>
        <ul class="list-group">
<?php
  $query = "SELECT name, distance FROM ROAD";
  $res = query($query);
  while($out = mysqli_fetch_array($res)) {
?>
          <li class="list-group-item"><?php echo $out['name'] . ": ". $out['distance']." km"; ?>
            <button type="button" class="btn btn-danger pull-right">Delete</button>
          </li>
<?php } ?>
        </ul>
      </div> 

    </div>  
  </body>

    <script>
      var fromOrTo = true;

      function myMap() {
        var myCenter = new google.maps.LatLng(23.7803829,90.414792);
        var mapCanvas = document.getElementById("map");
        
        var mapOptions = {
          center: myCenter,
          zoom: 8,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(mapCanvas, mapOptions);

        var marker = new google.maps.Marker({
          position: new google.maps.LatLng(23.7803829,90.414792),
          draggable: true
        });
        marker.setMap(map);

        // Zoom to 9 when clicking on marker
        google.maps.event.addListener(marker,'click',function() {
          map.setZoom(9);
          map.setCenter(marker.getPosition());
        });
        
        google.maps.event.addListener(marker, 'dragend', function (evt) {
          if (fromOrTo == true) {
            document.getElementById('lat_from').value = evt.latLng.lat();
            document.getElementById('long_from').value = evt.latLng.lng();
          } else {
            document.getElementById('lat_to').value = evt.latLng.lat();
            document.getElementById('long_to').value = evt.latLng.lng();
          }
          });
      }

      function toggle() {
        if (fromOrTo == true) {
          fromOrTo = false;
          document.getElementById('togglemarker').innerHTML = "Set location (From)";
        }
        else {
          fromOrTo = true;
          document.getElementById('togglemarker').innerHTML = "Set location (To)";
        }
      }
    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBw-88SCztPUR3LydCK3DC0HhgE16K2yAw&callback=myMap"></script>

</html>