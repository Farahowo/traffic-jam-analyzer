# DISCLAIMER: Due to GitLab only allowing the repository to be viewed under the Maintainer's account and not the Developers' account this repository has been cloned and reuploaded to show up under all Developers' repository list.
# Web Based Traffic Jam Analyzer with Real Time Update
*CSE482 Project* 
## A responsive website which is populated by real time traffic jam updates and predictions.

**Features**

- Getting real-time updates on blocked streets 24/7.
- Notifying other people by letting them know which roads are jam packed.
- Users and traffic control room both can provide traffic jam data to populate the real time traffic database.
- A sidebar will always show updates of severe and unusual jams.
- Community Forum where users can post query about traffic information on roads.
- Users of this forum can give updates through comment/post.
- Users providing more traffic input will get higher user ratings.
- Priority of users in the forum will be sorted according to user rating.
- AI based prediction where the AI in the backend will learn about each and every traffic jam input, location, time, duration and other things.
- The prediction and suggestion of the AI will be dynamic rather than static based on real time data.
- It will also consider data variation due to government holidays, weekends and other issues.
- Embedded Google Map with traffic jam marker.
- Users can input “source” and “destination” and get- \
Shortest/best path suggestion (based on real time data).\
Shortest/best path suggestion (based on AI’s prediction).\
Possibility of having jam at a certain time and at certain location (based on AI’s learning).\
Strategic suggestion based on Game Theory.\

**Developers' names-**
1. Md. Asimuzzaman Mansib (@AMansib)
2. Farah Hossain (@Farahowo)
