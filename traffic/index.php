<?php
  require_once("../includes/head.php");

  $mismatch = false;

  if (isset($_POST['submit'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];

    $query = "SELECT password, username FROM TRAFFIC_POLICE WHERE username = '$username'";
    $res = query($query);
    $out = mysqli_fetch_array($res);

    if ($password == $out[0]) {
      $_SESSION['traffic'] = $out[1];
      header("refresh:0");
    } else
      $mismatch = true;
  }

  $TRAFFIC = NULL;

  if (isset($_GET['mode'])) {
    if ($_GET['mode'] == "logout") {
      $_SESSION['traffic'] = NULL;
      header("Location: /traffic");
    }
  }

  if (isset($_SESSION['traffic'])) {
    $TRAFFIC = $_SESSION['traffic'];
  }

  if (isset($_POST['notice'])) {
    $title = escape($_POST['title']);
    $body = escape($_POST['body']);
    $time = time();

    switch ($_POST['exp_time']) {
      case 15:
        $expire = strtotime("15 minute");
        break;
      
      case 30:
        $expire = strtotime("30 minute");
        break;

      case 60:
        $expire = strtotime("1 hour");
        break;

      case 12:
        $expire = strtotime("12 hour");
        break;

      case 24:
        $expire = strtotime("1 day");
        break;

      case 7:
        $expire = strtotime("1 week");
        break;

      case 31:
        $expire = strtotime("1 month");
        break;

      default:
        $expire = time();
        break;
    }

    $query = "INSERT INTO NOTICE VALUES (NULL,'$title','$TRAFFIC','$body',$time,$expire)";
    query($query);
  }

  //TEMPORARY JAM addListener
  $place_name = "not found";
  if (isset($_POST['addjam'])) {
    $lat = $_POST['lat'];
    $lng = $_POST['lng'];

    $query = "SELECT place_id, name FROM PLACE WHERE lat_from < $lat AND lat_to > $lat AND long_from < $lng AND long_to > $lng";
    $res = query($query);
    $out = mysqli_fetch_array($res);

    $place_name = $out['name'];
    $place_id = $out['place_id'];
    $time = time();

    $query = "INSERT INTO JAM VALUES (NULL, $place_id, $time, NULL, 2)";
    query($query);

    $query = "SELECT * FROM SUBSCRIBE WHERE place_id = $place_id";
    $res = query($query);
    while ($out = mysqli_fetch_array($res)) {
      $user_id = $out['USER_ID'];
      $time = time();

      $query = "INSERT INTO NOTIFICATION VALUES (NULL,$user_id,$time,0,$place_id,2)";
      query($query);
    }
  } elseif (isset($_POST['stopjam'])) {
    $jam_id = $_POST['jam_id'];
    $time = time();

    $query = "UPDATE JAM SET end_time = $time WHERE jam_id = $jam_id";
    //echo $query;
    query($query);
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Traffic Police Login</title>

    <!-- Bootstrap -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>
  </head>
  
  <body>
    <nav class="navbar navbar-inverse navbar-fixed-top"> 
    
      <div class="container-fluid">

        <div class="navbar-header">

          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>

          <a class="navbar-brand" href="/">TrafficNow</a> 
        </div>

        <div class="collapse navbar-collapse" id="myNavbar">

          <ul class="nav navbar-nav">
            <li><a href="#">Home</a></li> 
            <li><a href="/live">Live Traffic</a></li>
            <li><a href="#">Prediction</a></li>
            <li><a href="#">Direction</a></li>
            <!--<li><a href="#">Forum</a></li>-->
            <li><button type="button" class="btn" style="background-color: #41873a; color: #ffffff;border-radius: 24px; margin-top: 9px;">Add Jam</button></li>
          </ul>

          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo strtoupper($TRAFFIC); ?> <!--<span class="glyphicon glyphicon-triangle-bottom"></span>--></a>
              <!--<ul class="dropdown-menu">
                <li><a href="#">Edit Profile <span class="glyphicon glyphicon-pencil"></span></a></li>
                <li><a href="#">My Posts <span class="badge">10</span></a></li>
                <li><a href="#">Notifications <span class="badge">5</span></a></li>
                <li><a href="#">Settings <span class="glyphicon glyphicon-cog"></span></a></li>
              </ul>-->
              </li>
            <li><a href="/traffic?mode=logout"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
          </ul>

        </div>

      </div>
    
    </nav>
<?php
  if (isset($TRAFFIC)) {
?>
    
    <!-- Navbar Ends -->
<?php } ?>

    <div class="container center-div" style="margin-top: 150px;">
<?php if(!isset($TRAFFIC)) { ?>
      <div class="col-lg-6 col-lg-offset-4 col-md-5 col-sm-8">
      <!-- Sign In --> 
              
        <form class="form-horizontal col-lg-6 col-md-5 col-sm-8" action="/traffic/index.php" method="post">
        <h3 align="center">Traffic Police login</h3><hr>
<?php
  if($mismatch) {
?>
  <div class="alert alert-danger fade in">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    Incorrect username or password.
  </div>
<?php } ?>
          <div class="form-group">
            <label for="username">Username:</label>
            
              <input type="text" class="form-control" id="username" name="username" placeholder="Enter Username">
          </div>

          <div class="form-group">
            <label for="pwd">Password:</label>
            
              <input type="password" class="form-control" id="pwd" name="password" placeholder="Enter password">
          </div>   

          <div class="form-group">
            <input type="submit" name="submit" class="btn btn-success btn-lg btn-block" value="LOGIN">
          </div>

          <div class="form-group">
            <a href="/" class="btn btn-primary">Go Back</a>
          </div>
        </form>
      </div>
<?php } else { ?>

      <div class="col-lg-12">
      
              
        <!--<form class="form-horizontal col-lg-4 col-md-6 col-sm-8" action="/traffic/index.php" method="post">-->
          <form class="form-horizontal col-md-4" action="/traffic/index.php" method="post">
          <h3><strong>Post Notice</strong></h3>         
          <div class="form-group">
            <label for="title">Title:</label>
            
              <input type="text" class="form-control" id="title" name="title" placeholder="Enter Title">
          </div>

          <div class="form-group">
            <label for="body">Description:</label>
            
              <textarea class="form-control" rows="5" id="body" name="body" placeholder="Description here"></textarea>
          </div>

          <!--<div class="form-group">
            <label for="exp_time">Expiration Time:</label>
            
              <input type="text" class="form-control" id="exp_time" placeholder="Enter Expiration Time">
          </div>-->

          <div class="form-group">
            <label for="exp_time">Expires in:</label>
            <select class="form-control" id="exp_time" name="exp_time">
              <option selected>None</option>
              <option value="15">15 minutes</option>
              <option value="30">30 minutes</option>
              <option value="60">1 hour</option>
              <option value="12">12 hours</option>
              <option value="24">1 day</option>
              <option value="7">1 week</option>
              <option value="31">1 month</option>
            </select>
        </div>

          <div class="form-group">
            <input type="submit" name="notice" class="btn btn-success btn-lg btn-block" value="POST NOTICE">
          </div>

        </form> <?php echo $place_name; ?>
        <div id="map" style="width:100%;height:500px;border-style: double; border-radius: 15px;"></div> <!--Map goes here-->

        <form class="form-horizontal col-md-6" style="margin: 4px;" action="/traffic/index.php" method="post">
          <input type="text" name="lat" id="lat">
          <input type="text" name="lng" id="lng">
<?php
  $jam_ended = false;
  $query = "SELECT * FROM JAM";
  $res = query($query);

  while($out = mysqli_fetch_array($res)){ //NOT COMPLETED
    $end_time = $out['END_TIME'];
    $jam_id = $out['JAM_ID'];
  }
  if ($end_time == NULL) {
?>
          <input type="hidden" name="jam_id" value="<?php echo $jam_id; ?>">
          <input type="submit" name="stopjam" class="btn btn-danger btn-lg btn-block" value="End Jam">
<?php
  } else {
?>
          <input type="submit" name="addjam" class="btn btn-primary btn-lg btn-block" value="Add Jam">
<?php } ?>          
        </form>

      </div>
<?php } ?>
    </div>
  </body>
  <script>
      function myMap() {
        var myCenter = new google.maps.LatLng(23.7803829,90.414792);
        var mapCanvas = document.getElementById("map");
        
        var mapOptions = {
          center: myCenter,
          zoom: 12,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(mapCanvas, mapOptions);

        var marker = new google.maps.Marker({
          position: new google.maps.LatLng(23.7803829,90.414792),
          draggable: true
        });
        marker.setMap(map);

        // Zoom to 9 when clicking on marker
        google.maps.event.addListener(marker,'click',function() {
          map.setZoom(9);
          map.setCenter(marker.getPosition());
        });
        
        google.maps.event.addListener(marker, 'dragend', function (evt) {
            document.getElementById('lat').value = evt.latLng.lat();
            document.getElementById('lng').value = evt.latLng.lng();
          });
      }

      /*function toggle() {
        if (fromOrTo == true) {
          fromOrTo = false;
          document.getElementById('togglemarker').innerHTML = "Set location (From)";
        }
        else {
          fromOrTo = true;
          document.getElementById('togglemarker').innerHTML = "Set location (To)";
        }
      }*/
    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBw-88SCztPUR3LydCK3DC0HhgE16K2yAw&callback=myMap"></script>

</html>