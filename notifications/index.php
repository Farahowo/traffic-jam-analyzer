<?php
  require_once("../includes/head.php");

  if ($USERID == NULL)
    jump("/sign-in-up?error=notuser");

  $query = "UPDATE NOTIFICATION SET is_seen = 1 WHERE user_id = $USERID";
  query($query);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <style type="text/css">
      .notification {
        padding: 15px;
        background-color: #fafafa;
        border-left: 6px solid #7f7f84;
        margin-bottom: 10px;
        -webkit-box-shadow: 0 5px 8px -6px rgba(0,0,0,.2);
           -moz-box-shadow: 0 5px 8px -6px rgba(0,0,0,.2);
                box-shadow: 0 5px 8px -6px rgba(0,0,0,.2);
        }
          
        .notification-sm {
          padding: 10px;
          font-size: 80%;
        }
        .notification-lg {
          padding: 35px;
          font-size: large;
        }
        .notification-success {
            border-color: #80D651;
        }
        .notification-success>strong {
            color: #80D651;
        }
        .notification-info {
            border-color: #45ABCD;
        }
        .notification-info>strong {
            color: #45ABCD;
        }
        .notification-warning {
            border-color: #FEAF20;
        }
        .notification-warning>strong {
            color: #FEAF20;
        }
        .notification-danger {
            border-color: #d73814;
        }
        .notification-danger>strong {
            color: #d73814;
        }

        .notification-primary {
            border-color: #428bca;
        }
        .notification-primary>strong {
            color: #428bca;
        }

      </style>
    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Notification Page</title>

    <!-- Bootstrap -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>
  </head>
 
  <body>
<?php require_once("../includes/header.php"); ?>

    <div class="container" style="margin-top: 100px;">

      <h1>NOTIFICATIONS</h1>
      <hr>
      
<?php
  $colors1 = array("notification notification-danger","notification notification-info",
    "notification notification-warning","notification notification-primary");
  $colors2 = array("d73814","45ABCD","FEAF20","428bca");

  $query = "SELECT * FROM NOTIFICATION WHERE user_id = $USERID ORDER BY time DESC";
  $res = query($query);
  while($out = mysqli_fetch_array($res)) {
    $index = rand(0,3);

    if ($out['TYPE'] == 1) { //1 for "comment on a post"
      $content = explode(",", $out['CONTENT']); //commenter id

      $query = "SELECT first_name, last_name FROM `USER` WHERE user_id = " . $content[0];
      $res2 = query($query);
      $out2 = mysqli_fetch_array($res2);
?>
      <div class="<?php echo $colors1[$index]; ?>">
        <a href="/community/post?pid=<?php echo $content[1]; ?>" style="color: inherit; text-decoration: none;"><strong style= "color: #<?php echo $colors2[$index]; ?>"><?php echo $out2[0]." ".$out2[1]; ?></strong> commented on your post. <span class="pull-right"><?php echo date("d M, h:ia", $out['TIME']); ?></span></a>
      </div>
<?php
    } elseif($out['TYPE']==2) { //2 for "jam subscription"
        $query = "SELECT p.name, a.name FROM PLACE p JOIN AREA a ON p.area_id = a.area_id WHERE p.place_id = " . $out['CONTENT'];
        $res2 = query($query);
        $out2 = mysqli_fetch_array($res2);
?>
      <div class="<?php echo $colors1[$index]; ?>">
        <a href="/live" style="color: inherit; text-decoration: none;"><strong style= "color: #<?php echo $colors2[$index]; ?>"><?php echo $out2[0].", ".$out2[1]; ?></strong> is going through traffic jam. <span class="pull-right"><?php echo date("d M, h:ia", $out['TIME']); ?></span></a>
      </div>
<?php }
} ?>
      <!--<div class="notification notification-danger">
          <a href="#" style="color: inherit; text-decoration: none;"><strong style= "color: #d73814">Title?</strong> hghghhghghgh <span class="pull-right">35 MINS AGO</span></a>
      </div>
      <div class="notification notification-info">
          <a href="#" style="color: inherit; text-decoration: none;"><strong style= "color: #45ABCD">Title?</strong> hghghhghghgh <span class="pull-right">35 MINS AGO</span></a>
      </div>
      <div class="notification notification-warning">
          <a href="#" style="color: inherit; text-decoration: none;"><strong style= "color: #FEAF20">Title?</strong> hghghhghghgh <span class="pull-right">35 MINS AGO</span></a>
      </div>
      <div class="notification notification-primary">
          <a href="#" style="color: inherit; text-decoration: none;"><strong style= "color: #428bca">Title?</strong> hghghhghghgh <span class="pull-right">35 MINS AGO</span></a>
      </div>
      <div class="notification notification-warning">
          <a href="#" style="color: inherit; text-decoration: none;"><strong style= "color: #FEAF20">Title?</strong> hghghhghghgh <span class="pull-right">35 MINS AGO</span></a>
      </div>-->
      
  
    </div> 
    
  </body>
</html>