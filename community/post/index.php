<?php
  require_once("../../includes/head.php");

  if (isset($_GET['pid'])) {
    $post_id = $_GET['pid'];

    $query = "SELECT * FROM POST WHERE post_id = $post_id";
    $res = query($query);
    $out = mysqli_fetch_array($res);

    $query = "SELECT COUNT(COMMENT_ID) FROM COMMENT WHERE post_id = $post_id";
    $res = query($query);
    $out2 = mysqli_fetch_array($res);
    $num_of_comments = $out2[0];

    /*$author = $out['AUTHOR_ID'];
    $query = "SELECT username FROM `USER` WHERE user_id = $author";
    $res = query($query);
    $out2 = mysqli_fetch_array($res);
    $author = $out2[0];*/

    function getUsername($id)
    {
      $query = "SELECT username FROM `USER` WHERE user_id = $id";
      $res = query($query);
      $out2 = mysqli_fetch_array($res);

      return $out2[0];
    }
  }

  $success = false;
  if (isset($_POST['submit'])) {
    $content = $_POST['content'];
    $time = time();

    $query = "INSERT INTO COMMENT VALUES (NULL,'$content',$USERID,$time,$post_id)";
    query($query);
    echo $query;
    $success = true;

    $query = "INSERT INTO NOTIFICATION VALUES (NULL,".$out['AUTHOR_ID'].",$time,0,'$USERID,$post_id',1)";
    query($query);
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <style type="text/css">
      .arrow-up {
        width: 0; 
        height: 0; 
        border-left: 20px solid transparent;
        border-right: 20px solid transparent;
        
        border-bottom: 20px solid #428bca;
      }

      .arrow-down {
        width: 0; 
        height: 0; 
        border-left: 20px solid transparent;
        border-right: 20px solid transparent;
        
        border-top: 20px solid #d9534f;
      }

      /* Extra small devices (phones, up to 480px) */
      @media screen and (max-width: 767px) {
        #uparrow{
          font-size: 35px;
        }

        #downarrow{
          font-size: 35px;
        }

        #posttitle{
          font-size: 20px;
        }

        #vote{
          font-size: 12px
        }

        #report{
          font-size: 20px;
        }

      }
      /* Small devices (tablets, 768px and up) */
      @media (min-width: 768px) and (max-width: 991px) {
      }
      /* tablets/desktops and up ----------- */
      @media (min-width: 992px) and (max-width: 1199px) {
        #uparrow{
          font-size: 4.5vw;
        }

        #downarrow{
          font-size: 4.5vw;
        }

        #report{
          font-size: 2.5vw;
        }
      }
      /* large desktops and up ----------- */
      @media screen and (min-width: 1200px) {
        #uparrow{
          font-size: 4.5vw;
        }

        #downarrow{
          font-size: 4.5vw;
        }

        #report{
          font-size: 2.5vw;
        }
      }


    </style>

    <script type="text/javascript">
      $(document).ready(function(){
      $('[data-toggle="tooltip"]').tooltip();
      });
    
    </script>

    <script type="text/javascript">
      function vote(updown,uid,pid) {
        var xhttp = new XMLHttpRequest();

        xhttp.onreadystatechange= function() {
          if (this.readyState == 4 && this.status == 200) {
            var xmlDoc = this.responseXML;
            
            var value = xmlDoc.getElementsByTagName('value');
            var count = xmlDoc.getElementsByTagName('count');

              document.getElementById("counter").innerHTML = count[0].childNodes[0].nodeValue;
          }
        };
        xhttp.open("GET", "/community/post/ajax.php?vote="+updown+"&uid="+uid+"&pid="+pid, true);
        xhttp.send();

        if (updown == "up")
          document.getElementById("upbutton").disabled = true;
        else
          document.getElementById("downbutton").disabled = true;
      }
    </script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $out['TITLE']; ?></title>

    <!-- Bootstrap -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>
  </head>
 
  <body>
<?php require_once("../../includes/header.php") ?>

    <div class="container" style="margin-top: 100px;">
      
      <div class="row"> <!-- VOTES ARE HERE! -->
        <div class="col-md-1 col-md-offset-0 col-xs-1 col-xs-offset-0">
          <div class="col-md-0 col-md-offset-0">
          <button data-toggle="tooltip" onclick="vote('up',<?php echo $USERID.",$post_id"; ?>)" id="upbutton" title="Upvote This Post!" type="button" class="btn btn-default btn-sm" style="background: none; border: none; box-shadow: none;"><span id= "uparrow" class="glyphicon glyphicon-triangle-top" style="color: #428bca;"></span></button>
          </div>
          
          <div id= "vote" class="col-md-1 col-md-offset-1 col-xs-0"><strong id="counter"><?php echo $out['VOTE']; ?></strong></div> 
          
          <div><button data-toggle="tooltip" onclick="vote('down',<?php echo $USERID.",$post_id"; ?>)" id="downbutton" title="Downvote This Post!" type="button" class="btn btn-default btn-sm" style="background: none; border: none; box-shadow: none;" ><span id= "downarrow" class="glyphicon glyphicon-triangle-bottom" style="color:#d9534f;"></span></button>
          </div>

          <div><button data-toggle="tooltip" title="Report This Post!" type="button" class="btn btn-default btn-sm" style="background: none; border: none; box-shadow: none; padding-left:22px;" ><span id= "report" class="glyphicon glyphicon-alert"></span></button>
          </div>
            
        </div> 

        <div class="media col-md-9 col-md-offset-1 col-xs-8 col-xs-offset-2">   
          
          <a class="pull-left" href="#">
            <img class="media-object img-circle img-responsive" src="/res/avatar-300x300.png" width="120" height="120">
            <br>
            <span class="glyphicon glyphicon-bitcoin"></span >  :200
              
            <span class="glyphicon glyphicon-king"></span>  :300
          </a>
          


          <div class="media-body">
            <div class="row"> 
              <a><h3 id = "posttitle" class="media-heading col-lg-10"><?php echo $out['TITLE']; ?></h3></a>
                    
            </div> 

            <p><?php echo $out['BODY']; ?></p>

            <ul class="list-inline list-unstyled">
              <li>
                By<a href="/profile?uid=<?php echo $out['AUTHOR_ID']; ?>"><i> <?php echo getUsername($out['AUTHOR_ID']); ?></i></a>
              </li>
              <li>|</li>
              <li>
                <span><i class="glyphicon glyphicon-calendar"></i> <?php echo date("j-M-y, g:ia",$out['TIME']); ?></span>
              </li>
              <li>|</li>
              <li>
                <span><i class="glyphicon glyphicon-comment"></i> <?php echo $num_of_comments; ?> comments</span>
              </li>  
              
              <li>|</li>
              <li>
<?php
  $query = "SELECT name FROM LOCATION_TAG l JOIN AREA a ON l.area_id = a.area_id WHERE l.post_id = $post_id";
  $res = query($query);
  while($out2 = mysqli_fetch_array($res)) {
?>
                <span class="label label-info"><?php echo $out2['name']; ?></span>
<?php } ?>
              </li>
            </ul>
          </div>
        </div>

        <!--<div class="col-md-1 col-md-offset-0 col-xs-1 col-xs-offset-0">
          <div class="col-md-0 col-md-offset-0"><button type="button" class="btn btn-default btn-sm" style="background: none; border: none; box-shadow: none;" ><span id= "uparrow"class="glyphicon glyphicon-triangle-top" style="color: #428bca;"></span></button>
          </div>
        </div> --> 

        
      </div>
      <hr style="height: 1px; border: none; background-color: #E4B9B9;">
      
      <div>
        <h3 style="padding: 10px;">Comments:</h3> <!-- COMMENTS HERE -->

        <div class="col-md-9 col-md-offset-1">
<?php if($success) { ?>
  <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      Comment has been posted succesfully!
  </div>
<?php 
  }

  $query = "SELECT * FROM COMMENT WHERE post_id = $post_id ORDER BY time ASC";
  $res = query($query);
  while($out2 = mysqli_fetch_array($res)) {
?> 
          <div class="well">
            <div class="media">
              <div class="media-left media-top">
                <img src="/res/avatar-300x300.png" class="media-object" style="width:60px">
              </div>
              <div class="media-body">
                <ul class="list-unstyled list-inline">
                  <li>
                    <a href="/profile?uid=<?php echo $out2['AUTHOR_ID']; ?>"><i> <?php echo getUsername($out2['AUTHOR_ID']); ?></i></a>
                  </li>
                  <li>|</li>
                  <li>
                    <i> <?php echo date("j-M-y, g:ia",$out2['TIME']); ?></i>
                  </li>
                </ul>  
                
                <p style="padding-top: 5px"> <?php echo $out2['CONTENT']; ?> </p>
                  
                  
              </div>
            </div>
          </div>
<?php } ?>

        </div> 
      </div>
<?php if(isset($USERID)) { ?>

      <h3 class="col-md-9 col-md-offset-1" style="padding: 10px;">Leave a Comment:</h3> <!-- REPLY HERE --> 
      <form class="col-md-9 col-md-offset-1" action="/community/post/index.php?pid=<?php echo $post_id; ?>" method="post">
        
        <div class="form-group">
          
          <textarea class="form-control" rows="5" id="body" name="content" placeholder="Your comment here. Using explicit language will get you immediately banned."></textarea>
        </div>
        <div class="form-group">

          <input type="submit" class="btn btn-primary" name="submit" value="Post">
        </div>

        
      </form>
<?php } ?>  
    </div> 
    
  </body>
</html>
