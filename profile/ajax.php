<!DOCTYPE html>
<html>
<head>
	<script type="text/javascript">
		function show() {
		  var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange= function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	var xmlDoc = this.responseXML;
		    	var x = xmlDoc.getElementsByTagName('reported');

		      	document.getElementById("ajax").innerHTML = x[0].childNodes[0].nodeValue;
		    }
		  };
		  xhttp.open("GET", "/res/response.php", true);
		  xhttp.send();
		}
	</script>
	<title></title>
</head>
<body>
	<h1 id="ajax"></h1>
	<button type="button" onclick="show()">Click</button>
</body>
</html>