<?php
  require_once("../includes/head.php");
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Live Traffic</title>

    <!-- Bootstrap -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>
    <script type="text/javascript">
      function setMarker(map) {
        var xhttp = new XMLHttpRequest();

        xhttp.onreadystatechange= function() {
          if (this.readyState == 4 && this.status == 200) {
            var xmlDoc = this.responseXML;
            var results = xmlDoc.getElementsByTagName("jam");

            for (var i = 0; i < results.length; i++) {
              var lat = results[i].childNodes[1].firstChild.nodeValue;
              var lng = results[i].childNodes[2].firstChild.nodeValue;

              var point = new google.maps.LatLng(lat,lng);
              var marker1 = new google.maps.Marker({position: point,
                                                    icon: {
                                                        path: google.maps.SymbolPath.CIRCLE,
                                                        scale: 9,
                                                        strokeColor: 'white',
                                                        strokeOpacity: 0.8,
                                                        strokeWeight: 1,
                                                        
                                                        fillColor: 'red',
                                                        fillOpacity: .5, }});
              marker1.setMap(map);
            }
          }
        };

        xhttp.open("GET", "/live/ajax.php?name=100", true);
        xhttp.send();       
      }

      function listJams(key) {
        var xhttp = new XMLHttpRequest();

        xhttp.onreadystatechange= function() {
          if (this.readyState == 4 && this.status == 200) {
            var xmlDoc = this.responseXML;
            var results = xmlDoc.getElementsByTagName("jam");

            var ul = document.getElementById('jams');
            ul.innerHTML = "";

            for (var i = 0; i < results.length; i++) {
              var place = results[i].childNodes[0].firstChild.nodeValue;
              var lat = results[i].childNodes[1].firstChild.nodeValue;
              var lng = results[i].childNodes[2].firstChild.nodeValue;
              var time = results[i].childNodes[3].firstChild.nodeValue;
              var type = results[i].childNodes[4].firstChild.nodeValue;

              var bt = document.createElement('LI');
              //bt.setAttribute('type', 'button');        
              bt.className = "list-group-item";
              //bt.onclick = function(){setCenter(lat,lng)};

              var content = "<h4 class=\"list-group-item-heading\">"+place+"</h4>";
        
              if (time > 59) {
                content += "<p class=\"list-group-item-text\">"+Math.floor(time/60)+" hours ";
                content += Math.floor(time%60)+ " mins</p>";
              } else {
                content += "<p class=\"list-group-item-text\">"+time+" mins</p>";
              }

              content += "<ul class=\"list-unstyled list-inline list-group-item-text\">";
              content += "<li>36 "+"user inputs</li><li>|</li>";

              if (type == 1) {
                content += "<li><span class=\"glyphicon glyphicon-star\"></span></li>";
              }
              content += "<button type=\"button\" class=\"btn btn-default btn-sm\" onclick=\"setCenter("+lat+","+lng+")\">";
              content += "<span class=\"glyphicon glyphicon-map-marker\"></span></button>";
              
              content += "</ul>";

              bt.innerHTML = content;
              ul.appendChild(bt);
            }
          }
        };

        xhttp.open("GET", "/live/ajax.php?name="+key, true);
        xhttp.send();
      }
    </script>
  </head>
  
  <body onload="listJams(100)">
<?php require_once("../includes/header.php"); ?>
    <div class="container center-div" style="margin-top: 80px">
      <div id="map" style="width:100%;height:400px"></div>

        <script>
          var map;

          function myMap() {
            var myCenter = new google.maps.LatLng(23.7450105,90.3818806);
            var mapCanvas = document.getElementById("map");
            var mapOptions = {center: myCenter, 
                              zoom: 12, 
                              mapTypeId: google.maps.MapTypeId.ROADMAP,
                              //disableDefaultUI: true
                              mapTypeControl: false,
                              streetViewControl: false};
            /*var*/ map = new google.maps.Map(mapCanvas, mapOptions);

            setMarker(map);
            }

            function setCenter(latval,longval) {
              map.setCenter({
                lat : latval,
                lng : longval
              });
              map.setZoom(15);
            }
          </script>
          <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBw-88SCztPUR3LydCK3DC0HhgE16K2yAw&callback=myMap"></script>

          <form  class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1" style="margin-top: 20px;">
    
            <input type="text" class="form-control" onkeyup="listJams(this.value)" placeholder="Search here">

            <div style="margin-top: 10px; max-height: 200px; overflow-y:scroll;">
              <ul class="list-group" id="jams">
                <!--<li class="list-group-item" style="background-color: #ef412b;">
                  <h4 class="list-group-item-heading">Place Name</h4>
                  <p class="list-group-item-text">26 mins</p>
                  <ul class="list-unstyled list-inline list-group-item-text">
                    <li>36 user inputs</li>
                    <li>|</li>
                    <li><span class="glyphicon glyphicon-star"></span></li>  
                  </ul>  
                </li>-->
              </ul>
            </div>
          </form>  
    </div>
  </body>

</html>