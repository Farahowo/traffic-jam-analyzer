<?php
	require_once("../includes/database.php");

	ConnectDb();

	header('Content-Type: text/xml');
	$output = "<?xml version=\"1.0\"?>\n";
	echo $output;

	if (isset($_GET['name'])) {
		$name = $_GET['name'];

		if ($name == "100") {
			$query = "SELECT p.lat_from, p.long_from, p.lat_to, p.long_to, j.start_time, p.name, j.type FROM JAM j JOIN PLACE p ON j.place_id=p.place_id WHERE j.end_time IS NULL";
		} else {
			$query = "SELECT p.lat_from, p.long_from, p.lat_to, p.long_to, j.start_time, p.name, j.type FROM JAM j JOIN PLACE p ON j.place_id=p.place_id WHERE j.end_time IS NULL AND p.name LIKE '%$name%'";
		}

		$res = query($query);

		echo "<output>";
		while($out = mysqli_fetch_array($res)) {
			$duration = round((time() - $out['start_time'])/60);
			
			echo "<jam>";
			
			$lat = ($out['lat_from']+$out['lat_to'])/2;
			$long = ($out['long_from']+$out['long_to'])/2;
			echo "<place>".$out['name']."</place>";
			echo "<lat>$lat</lat>";
			echo "<long>$long</long>";
			echo "<time>$duration</time>";
			echo "<type>".$out['type']."</type>";

			echo "</jam>";
		}

		echo "</output>";
	}
?>