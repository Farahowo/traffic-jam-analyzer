<?php require_once("includes/head.php"); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <style type="text/css">
      #section1 {
        border-left: 6px solid #490921;
        margin-bottom: 30px;
        background-color: #fafafa;
        -webkit-box-shadow: 0 5px 8px -6px rgba(0,0,0,.5);
           -moz-box-shadow: 0 5px 8px -6px rgba(0,0,0,.5);
                box-shadow: 0 5px 8px -6px rgba(0,0,0,.5);
      }
      #section2 {
        border-left: 6px solid #8d0011;
        margin-bottom: 30px;
        background-color: #fafafa;
        -webkit-box-shadow: 0 5px 8px -6px rgba(0,0,0,.5);
           -moz-box-shadow: 0 5px 8px -6px rgba(0,0,0,.5);
                box-shadow: 0 5px 8px -6px rgba(0,0,0,.5);
      }
      #section3 {
        border-left: 6px solid #FC9F3E;
        margin-bottom: 30px;
        background-color: #fafafa;
        -webkit-box-shadow: 0 5px 8px -6px rgba(0,0,0,.5);
           -moz-box-shadow: 0 5px 8px -6px rgba(0,0,0,.5);
                box-shadow: 0 5px 8px -6px rgba(0,0,0,.5);
      }
      #section4 {
        border-left: 6px solid #34aacf;
        margin-bottom: 30px;
        background-color: #fafafa;
        -webkit-box-shadow: 0 5px 8px -6px rgba(0,0,0,.5);
           -moz-box-shadow: 0 5px 8px -6px rgba(0,0,0,.5);
                box-shadow: 0 5px 8px -6px rgba(0,0,0,.5);
      }
    </style>
    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="refresh" content="60">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Home</title>

    <!-- Bootstrap -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <style type="text/css">
      body {
        /*background-image: url('http://kingofwallpapers.com/highway/highway-007.jpg');*/
        background-image: url('/res/traffic.jpg');
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-size: cover;
      }
    </style>    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>
  </head>
 
  <body>
<?php require_once("includes/header.php"); ?>

    <div class="container" style="margin-top: 100px;">

      
      <div class="row">
        <nav class="col-sm-3 hidden-xs" id="myScrollspy">
          <ul class="nav nav-pills nav-stacked">
<?php
  $notice_exists = false;
  $query = "SELECT * FROM NOTICE WHERE lifetime > ". time()." ORDER BY time DESC";
  $notices = query($query);
  if(mysqli_num_rows($notices) > 0) {
    $notice_exists = true;
?>
            <li class="active"><a href="#section1" style="color: #d1ccce; text-decoration: none; background-color: #490921">Announcements</a></li>
<?php }
  if ($USERNAME != NULL) {
?>
            <li><a href="#section2" style="color: #d1ccce; text-decoration: none; background-color: #8d0011">Traffic Updates</a></li>
<?php
  }
?>
            <li><a href="#section3" style="color: #E9EAE6; text-decoration: none; background-color: #FC9F3E">Hot/Trending Discussions</a></li>
            <li><a href="#section4" style="color: #E9EAE6; text-decoration: none; background-color: #34aacf">Recent Posts</a></li>
          </ul>
        </nav>

        <div class="col-sm-9">

<?php if($notice_exists) { ?>          
          <div id="section1" style="opacity: 0.9; border-radius: 15px;">
<?php
  while($out = mysqli_fetch_array($notices)) {
?>
            <div class="well well-sm" style="margin: 10px;">
              <h2 style="color: #490921"><?php echo $out['TITLE']; ?></h2>      
              <p><?php echo $out['CONTENT']; ?></p>
              <p><i><?php echo "Posted on ".date("d-M-y, h:ia", $out['TIME']).", Expires in ". floor((($out['LIFETIME']-time())/60))." mins"; ?></i></p>
            </div>
<?php } echo "</div>";} 
  if($USERNAME != NULL) {
?>

          <div id="section2" style="opacity: 0.9; border-radius: 15px;">
            <h2 style="margin: 10px; color: #8d0011;">Traffic Jam Updates</h2>
<?php
  $query = "SELECT j.start_time, p.name, a.name FROM JAM j JOIN PLACE p ON j.place_id = p.place_id ";
  $query .= "JOIN AREA a ON p.area_id = a.area_id WHERE j.end_time IS NULL ORDER BY j.start_time";
  $res = query($query);
  while($out = mysqli_fetch_array($res)) {
?>
            <div style="margin: 10px;" class="well well-sm">
              <a href="/live" style="color: inherit; text-decoration: none;"><strong style="font-size: 15px;"><?php echo $out[1].", ".$out[2]; ?></strong><span class="pull-right"><?php echo round((time()-$out[0])/60); ?> mins ago</span></a>
            </div>
<?php } ?>
          </div>
<?php } ?>    

          <div id="section3" style="opacity: 0.9; border-radius: 15px;"> 
            <h2 style="margin: 10px; color: #FC9F3E;">Hot/Trending Discussions</h2>
<?php
  $query = "SELECT p.post_id,p.title,p.body,p.time,u.username,u.user_id FROM POST p JOIN `USER` u ON p.author_id = u.user_id LIMIT 5";
  $res = query($query);
  while($post = mysqli_fetch_array($res)) {
?>     
            <div class="media well well-md" style="margin: 10px;">
              <a class="pull-left" href="#">
                <img class="media-object img-circle img-responsive" src="http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png" width="70" height="70">
              </a>

              <div class="media-body">
                <div class="row"> 
                  <a href="/community/post?pid=<?php echo $post['post_id']; ?>"><h4 class="media-heading col-lg-10"><?php echo $post['title']; ?></h4></a>
                </div> 

                <p><?php echo substr($post['body'], 0,100); ?></p>

                <ul class="list-inline list-unstyled">
                  <li>
                    By<a href="profile?uid=<?php echo $post['user_id']; ?>"><i> <?php echo $post['username']; ?></i></a>
                  </li>
                  <li>|</li>
                  <li>
                    <span><i class="glyphicon glyphicon-calendar"></i><?php echo date("d-M-y, h:ia", $post['time']); ?></span>
                  </li>
                  <li>|</li>
<?php
  $query = "SELECT COUNT(comment_id) FROM COMMENT WHERE post_id = ". $post['post_id'];
  $res1 = query($query);
  $comment = mysqli_fetch_array($res1);
?>
                    <span><i class="glyphicon glyphicon-comment"></i> <?php echo $comment[0]; ?> comments</span>
                  <li>|</li>
<?php
  $query = "SELECT SUM(value) FROM POST_VOTE WHERE post_id = ". $post['post_id'];
  $res2 = query($query);
  $vote = mysqli_fetch_array($res2);
  $vote = array_filter($vote);

  if (empty($vote)) {
    $vote[0] = 0;
  }
?>
                  <li>
                     <span><i class="glyphicon glyphicon-thumbs-up"></i> <?php echo $vote[0]; ?> Votes</span>
                  </li>
                </ul>
              </div>
            </div>
<?php } ?>
          </div>

          <div id="section4" style="opacity: 0.9; border-radius: 15px;">         
            <h2 style="margin: 10px; color: #34AACF">Recent Posts</h2>
<?php
  $query = "SELECT p.post_id,p.title,p.body,p.time,u.username,u.user_id FROM POST p JOIN `USER` u ON p.author_id = u.user_id";
  $query .= " ORDER BY p.time DESC LIMIT 5";
  $res = query($query);
  while($post = mysqli_fetch_array($res)) {
?>     
            <div class="media well well-md" style="margin: 10px;">
              <a class="pull-left" href="#">
                <img class="media-object img-circle img-responsive" src="http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png" width="70" height="70">
              </a>

              <div class="media-body">
                <div class="row"> 
                  <a href="/community/post?pid=<?php echo $post['post_id']; ?>"><h4 class="media-heading col-lg-10"><?php echo $post['title']; ?></h4></a>
                </div> 

                <p><?php echo substr($post['body'], 0,100); ?></p>

                <ul class="list-inline list-unstyled">
                  <li>
                    By<a href="profile?uid=<?php echo $post['user_id']; ?>"><i> <?php echo $post['username']; ?></i></a>
                  </li>
                  <li>|</li>
                  <li>
                    <span><i class="glyphicon glyphicon-calendar"></i><?php echo date("d-M-y, h:ia", $post['time']); ?></span>
                  </li>
                  <li>|</li>
<?php
  $query = "SELECT COUNT(comment_id) FROM COMMENT WHERE post_id = ". $post['post_id'];
  $res1 = query($query);
  $comment = mysqli_fetch_array($res1);
?>
                    <span><i class="glyphicon glyphicon-comment"></i> <?php echo $comment[0]; ?> comments</span>
                  <li>|</li>
<?php
  $query = "SELECT SUM(value) FROM POST_VOTE WHERE post_id = ". $post['post_id'];
  $res2 = query($query);
  $vote = mysqli_fetch_array($res2);
  $vote = array_filter($vote);

  if (empty($vote)) {
    $vote[0] = 0;
  }
?>
                  <li>
                     <span><i class="glyphicon glyphicon-thumbs-up"></i> <?php echo $vote[0]; ?> Votes</span>
                  </li>
                </ul>
              </div>
            </div>
<?php } ?>   
            <!--<div class="media well well-md" style="margin: 10px;">
              <a class="pull-left" href="#">
                <img class="media-object img-circle img-responsive" src="http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png" width="70" height="70">
              </a>

              <div class="media-body">
                <div class="row"> 
                  <a><h4 class="media-heading col-lg-10">FIND ME SOME BABY BUNNIES!</h4></a>
                </div> 

                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis pharetra varius quam sit amet vulputate.</p>

                <ul class="list-inline list-unstyled">
                  <li>
                    By<a href="#"><i> Username</i></a>
                  </li>
                  <li>|</li>
                  <li>
                    <span><i class="glyphicon glyphicon-calendar"></i> 2 days, 8 hours </span>
                  </li>
                  <li>|</li>
                    <span><i class="glyphicon glyphicon-comment"></i> 2 comments</span>
                  <li>|</li>
                  <li>
                     <span><i class="glyphicon glyphicon-thumbs-up"></i> 3 Votes</span>
                  </li>
                </ul>
              </div>
            </div>-->
          </div>      
        </div>
      </div>
  
    </div> 
    
  </body>
</html>