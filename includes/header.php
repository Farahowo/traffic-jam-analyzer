<?php
  if ($USERNAME == NULL) {
    //NOT LOGGED IN
?>

  <nav class="navbar navbar-inverse navbar-fixed-top"> 

    <div class="container-fluid">

      <div class="navbar-header">

        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>

        <a class="navbar-brand" href="/"><strong>TrafficNow</strong></a>       </div>

      <div class="collapse navbar-collapse" id="myNavbar">

        <ul class="nav navbar-nav">
          <li><a href="/">Home</a></li> <!-- class active wouldn't be on all the  time the php developer shuold take care to use it when he wishes. -->
          <li><a href="/live">Live Traffic</a></li>
          <li><a href="/community">Forum</a></li>
        </ul>

        <ul class="nav navbar-nav navbar-right">
          <li><a href="/sign-in-up#signup"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
          <li class="active"><a href="/sign-in-up"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
        </ul>

      </div>

    </div>
  
  </nav>
<?php
  } else {
?>
  <script type="text/javascript">
    function setNavbar(page_name) {
      if (page_name == "home") {
        document.getElementById("nav_home").className = "active";
      } else if (page_name == "forum") {
        document.getElementById("nav_forum").className = "active";
      }
    }
  </script>
    <nav class="navbar navbar-inverse navbar-fixed-top"> 
    <!-- If navbar-fixed-top is cut then the <h1> is shown. Else it cvers it. So <br> is used.--> 
    <!-- Navbar starts -->
      <div class="container-fluid">

        <div class="navbar-header">

          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>

          <a class="navbar-brand" href="/"><strong>TrafficNow</strong></a> <!-- Give this site a name please -->
        </div>

        <div class="collapse navbar-collapse" id="myNavbar">

		<script type="text/javascript"> 
		    function jamButton(is_onload) {
			  var add_jam = document.getElementById("add_jam");
			  //the database info will actually decide the color

			  if (is_onload == 0) {     
			    var current = add_jam.innerHTML;

			    if (current == "Add Jam") {
			      add_jam.style.backgroundColor = "#9b1f41";

			      if (navigator.geolocation) {
			        navigator.geolocation.getCurrentPosition(storeLocation);
			      }
			      add_jam.innerHTML = "Jam Running";

			    } else {
			      add_jam.style.backgroundColor = "#41873a";
			      add_jam.innerHTML = "Add Jam";
			    }
			  }
			}

			function storeLocation(location) {
				//Add jam input taker code here
        //location.coords.latitude;
        //location.coords.longitude;
			}
		</script>

          <ul class="nav navbar-nav">
            <li id="nav_home"><a href="/">Home</a></li> <!-- class active wouldn't be on all the  time the php developer shuold take care to use it when he wishes. -->
            <li><a href="/live">Live Traffic</a></li>
            <li><a href="/prediction">Prediction</a></li>
            <li><a href="/direction">Direction</a></li>
            <li id="nav_forum"><a href="/community">Forum</a></li>
            <li><button type="button" id="add_jam" onclick="jamButton(0)" class="btn" style="background-color: #41873a; color: #ffffff;border-radius: 24px; margin-top: 9px; font-weight: bold;">Add Jam</button></li>
          </ul>

          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo strtoupper($USERNAME); ?> <span class="glyphicon glyphicon-triangle-bottom"></span></a>
              <ul class="dropdown-menu">
                <li><a href="/profile/edit">Edit Profile <span class="glyphicon glyphicon-pencil"></span></a></li>
<?php
  $query = "SELECT COUNT(post_id) FROM POST WHERE author_id = $USERID";
  $res = query($query);
  $mypost = mysqli_fetch_array($res);

  $query = "SELECT COUNT(NT_ID) FROM NOTIFICATION WHERE user_id = $USERID AND is_seen = 0";
  $res = query($query);
  $mynoti = mysqli_fetch_array($res);
?>
                <li><a href="/community/my">My Posts <span class="badge"><?php echo $mypost[0]; ?></span></a></li>
                <li><a href="/notifications">Notifications <span class="badge"><?php echo $mynoti[0]; ?></span></a></li>
                <!--<li><a href="#">Settings <span class="glyphicon glyphicon-cog"></span></a></li>-->
                <li><a href="/profile/subscribe.php">Subscribe <span class="glyphicon glyphicon-unchecked"></span></a></li>
              </ul>
              </li>
            <li><a href="/sign-in-up?mode=logout"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
          </ul>

        </div>

      </div>
    
    </nav>
<?php
  }
?>